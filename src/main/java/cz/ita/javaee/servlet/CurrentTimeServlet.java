package cz.ita.javaee.servlet;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class CurrentTimeServlet extends GenericServlet {

    @Override
    public void service(ServletRequest request, ServletResponse response) throws
            ServletException, IOException {
        String time = new Date().toString();
        PrintWriter out = response.getWriter();
        out.println(time);
    }
}
