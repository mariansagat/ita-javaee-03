package cz.ita.javaee.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.*;
import java.util.Enumeration;

/**
 * Created by Majo on 29.5.2017.
 */
//@WebFilter(filterName = "LoggingFilter")
public class LoggingFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        String path = httpServletRequest.getServletPath();

        Enumeration<String> headersEnum = httpServletRequest.getHeaderNames();

        if (headersEnum != null) {
            while (headersEnum.hasMoreElements()) {
                String header = headersEnum.nextElement();
                Enumeration<String> values = ((HttpServletRequest) req).getHeaders(header);
                while (values.hasMoreElements()) {
                    LOGGER.info(values.nextElement());

                }

            }
        }

        PrintWriter out = resp.getWriter();
        LOGGER.info("path:" + path);
        String browser = ((HttpServletRequest) req).getHeader("User-Agent");
        LOGGER.info("browser: " + browser);


        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
