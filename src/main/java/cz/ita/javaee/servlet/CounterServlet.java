package cz.ita.javaee.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Majo on 29.5.2017.
 */
@WebServlet(name = "CounterServlet", value = "/counter")
public class CounterServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Object counterObject = session.getAttribute("counterState");

        int count = 1;

        if (counterObject == null) {
            session.setAttribute("counterState", 1);

        } else {
            count = (int) counterObject;
            count++;
            session.setAttribute("counterState", count);
        }

        PrintWriter out = response.getWriter();
        out.println("Number of requests: " + (int) session.getAttribute("counterState"));
        out.println("Number of requests: " + count);
    }
}
